package es.ecommerce.pricesapi.domain.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

import es.ecommerce.pricesapi.domain.model.persistence.PriceTable;
import es.ecommerce.pricesapi.infrastructure.repository.PricesJPARepository;
import java.util.Collections;
import java.util.Date;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class PricesDomainRepositoryTest {

  @Mock
  private PricesJPARepository pricesJPARepository;

  @InjectMocks
  private PricesDomainRepository pricesDomainRepository;

  @BeforeEach
  public void initialize() {
    openMocks(this);
  }

  @Test
  void should_find_by_product_id_and_brand_id_and_application_date() {
    var productId = 1L;
    var brandId = 1L;
    Date applicationDate = new Date();

    var priceTableExpected = PriceTable.builder()
        .build();

    var priceTableExpectedList = Collections.singletonList(priceTableExpected);

    when(pricesJPARepository.findByProductIdAndBrandIdAndApplicationDate(anyLong(), anyLong(),
        any(Date.class)))
        .thenReturn(priceTableExpectedList);

    var priceTableList = pricesDomainRepository
        .findByProductIdAndBrandIdAndApplicationDate(productId, brandId, applicationDate);

    assertThat(priceTableList)
        .isNotNull()
        .isNotEmpty()
        .hasSize(1)
        .containsExactly(priceTableExpected);
  }

}