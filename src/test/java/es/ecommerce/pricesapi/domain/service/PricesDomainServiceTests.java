package es.ecommerce.pricesapi.domain.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

import es.ecommerce.pricesapi.domain.repository.PricesRepository;
import es.ecommerce.pricesapi.infrastructure.converter.PricesMapper;
import es.ecommerce.pricesapi.domain.model.dto.PriceDto;
import es.ecommerce.pricesapi.domain.model.rest.PriceRequestDto;
import es.ecommerce.pricesapi.domain.model.dto.PriceServiceDto;
import es.ecommerce.pricesapi.domain.model.persistence.PriceTable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;


@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class PricesDomainServiceTests {

  private static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

  private static final DateFormat DATE_FORMAT =
      new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS);
  private static final DateTimeFormatter DATE_TIME_FORMATTER =
      DateTimeFormatter.ofPattern(YYYY_MM_DD_HH_MM_SS);

  @Mock
  private PricesMapper pricesMapper;

  @Mock
  private PricesRepository pricesRepository;

  @InjectMocks
  private PricesDomainService priceService;

  @BeforeEach
  public void initialize() {
    openMocks(this);
  }


  @Test
  void should_find_one() {
    var productId = 1L;
    var brandId = 1L;
    var price = BigDecimal.valueOf(12.34);
    var defaultStartDateString = "2020-01-01 12:34:56";
    var defaultEndDateString = "2020-01-01 23:45:00";

    var startZonedDateTime = getZonedDateTime(defaultStartDateString);
    var endZonedDateTime = getZonedDateTime(defaultEndDateString);

    var priceDto = PriceDto.builder()
        .brandId(brandId)
        .price(price)
        .productId(productId)
        .startDate(startZonedDateTime)
        .endDate(endZonedDateTime)
        .build();

    var priceEntity = PriceTable.builder()
        .id(0L)
        .priority(0L)
        .brandId(brandId)
        .productId(productId)
        .currency("EUR")
        .price(price)
        .startDate(getDate(defaultStartDateString))
        .endDate(getDate(defaultEndDateString))
        .build();

    var priceEntityList = Collections.singletonList(priceEntity);

    var priceRequestDto = PriceRequestDto.builder()
        .productId(productId)
        .brandId(brandId)
        .applicationDate(getDate(defaultStartDateString))
        .build();

    var applicationDate = getDate(defaultStartDateString);

    when(pricesRepository.findByProductIdAndBrandIdAndApplicationDate(productId, brandId,
        applicationDate)).thenReturn(priceEntityList);
    when(pricesMapper.fromTableToDto(priceEntity)).thenReturn(priceDto);

    PriceServiceDto priceServiceDto = PriceServiceDto.builder()
        .productId(productId)
        .brandId(brandId)
        .applicationDate(getDate(defaultStartDateString))
        .build();

    var actualDto = priceService.findOne(priceServiceDto);

    assertThat(actualDto)
        .isNotNull()
        .isEqualTo(PriceDto.builder()
            .brandId(brandId)
            .price(price)
            .productId(productId)
            .startDate(startZonedDateTime)
            .endDate(endZonedDateTime)
            .build());
  }


  @Test
  void should_return_max_priority_between_two_values() {
    var productId = 1L;
    var brandId = 1L;
    var price = BigDecimal.valueOf(12.34);
    var eurString = "EUR";
    var defaultStartDateString = "2020-01-01 12:34:56";
    var defaultEndDateString = "2020-01-01 23:45:00";

    var startDate = getDate(defaultStartDateString);
    var endDate = getDate(defaultEndDateString);

    var priceEntity0 = PriceTable.builder()
        .id(0L)
        .priority(0L)
        .brandId(brandId)
        .productId(productId)
        .currency(eurString)
        .price(price)
        .startDate(startDate)
        .endDate(endDate)
        .build();

    var priceEntity1 = PriceTable.builder()
        .id(1L)
        .priority(1L)
        .brandId(brandId)
        .productId(productId)
        .currency(eurString)
        .price(price)
        .startDate(startDate)
        .endDate(endDate)
        .build();

    var priceDto0 = PriceDto.builder()
        .brandId(brandId)
        .price(price)
        .productId(productId)
        .startDate(getZonedDateTime(defaultStartDateString))
        .endDate(getZonedDateTime(defaultEndDateString))
        .build();

    var priceDto1 = PriceDto.builder()
        .brandId(brandId)
        .price(price)
        .productId(productId)
        .startDate(getZonedDateTime(defaultStartDateString))
        .endDate(getZonedDateTime(defaultEndDateString))
        .build();

    var applicationDate = getDate(defaultStartDateString);

    var pricesEntityList = Arrays.asList(priceEntity0, priceEntity1);

    var priceRequestDto = PriceRequestDto.builder()
        .productId(productId)
        .brandId(brandId)
        .applicationDate(getDate(defaultStartDateString))
        .build();

    var priceServiceDto = PriceServiceDto.builder()
        .productId(productId)
        .brandId(brandId)
        .applicationDate(getDate(defaultStartDateString))
        .build();

    when(pricesRepository
        .findByProductIdAndBrandIdAndApplicationDate(1L, 1L, applicationDate))
        .thenReturn(pricesEntityList);
    when(pricesMapper.fromTableToDto(priceEntity0)).thenReturn(priceDto0);
    when(pricesMapper.fromTableToDto(priceEntity1)).thenReturn(priceDto1);

    var actualDto = priceService.findOne(priceServiceDto);

    assertThat(actualDto)
        .isNotNull()
        .isEqualTo(priceDto1);
  }


  public static ZonedDateTime getZonedDateTime(String dateString) {
    var localDate = LocalDate.parse(dateString, DATE_TIME_FORMATTER);
    return localDate.atStartOfDay(ZoneId.systemDefault());
  }

  @SneakyThrows
  public static java.util.Date getDate(String date) {
    return DATE_FORMAT.parse(date);
  }
}
