package es.ecommerce.pricesapi.application;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import es.ecommerce.pricesapi.PricesApiApplication;
import java.util.stream.Stream;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.MOCK,
    classes = PricesApiApplication.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class IntegrationTests {

  @Autowired
  MockMvc mockMvc;

  @ParameterizedTest
  @MethodSource("expectedInputAndOutputResults")
  void should_return_expected_value(String inputDate, String expectedStartDate,
      String expectedEndDate, Double expectedPrice) throws Exception {
    mockMvc.perform(MockMvcRequestBuilders
            .get("/prices")
            .params(buildRequestParams(inputDate))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.product_id", is(35455)))
        .andExpect(jsonPath("$.start_date", is(expectedStartDate)))
        .andExpect(jsonPath("$.end_date", is(expectedEndDate)))
        .andExpect(jsonPath("$.price", is(expectedPrice)));
  }

  private MultiValueMap<String, String> buildRequestParams(String inputDate) {
    MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
    map.add("application_date", inputDate);
    map.add("product_id", "35455");
    map.add("brand_id", "1");
    return map;
  }

  private static Stream<Arguments> expectedInputAndOutputResults() {
    return Stream.of(
        Arguments.of("2020-06-14 10:00:00", "2020-06-14 00:00:00", "2020-12-31 23:59:59", 35.50),
        Arguments.of("2020-06-14 15:00:00", "2020-06-14 15:00:00", "2020-06-14 18:30:00", 25.45),
        Arguments.of("2020-06-14 21:00:00", "2020-06-14 00:00:00", "2020-12-31 23:59:59", 35.50),
        Arguments.of("2020-06-15 10:00:00", "2020-06-15 00:00:00", "2020-06-15 11:00:00", 30.50),
        Arguments.of("2020-06-21 16:00:00", "2020-06-15 16:00:00", "2020-12-31 23:59:59", 38.95)
    );
  }

}
