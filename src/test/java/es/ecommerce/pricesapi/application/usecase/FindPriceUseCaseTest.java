package es.ecommerce.pricesapi.application.usecase;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

import es.ecommerce.pricesapi.infrastructure.converter.PricesMapper;
import es.ecommerce.pricesapi.domain.model.dto.PriceDto;
import es.ecommerce.pricesapi.domain.model.rest.PriceRequestDto;
import es.ecommerce.pricesapi.domain.model.dto.PriceServiceDto;
import es.ecommerce.pricesapi.domain.model.rest.PriceJSONDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class FindPriceUseCaseTest {

  @Mock
  private PricesMapper priceMapper;

  @Mock
  private es.ecommerce.pricesapi.domain.service.PricesService pricesService;

  @InjectMocks
  private FindPriceUseCase useCase;

  @BeforeEach
  public void initialize() {
    openMocks(this);
  }


  @Test
  void should_find_price() {
    var priceRequestDto = PriceRequestDto.builder()
        .build();

    var priceDto = PriceDto.builder()
        .build();

    var priceServiceDto = PriceServiceDto.builder()
        .build();

    var expectedPriceJSONDto = PriceJSONDto.builder()
        .build();

    when(priceMapper.fromRequestToService(priceRequestDto)).thenReturn(priceServiceDto);
    when(priceMapper.fromDtoToJSON(priceDto)).thenReturn(expectedPriceJSONDto);

    when(pricesService.findOne(priceServiceDto)).thenReturn(priceDto);

    var priceJSONDto = useCase.findPrice(priceRequestDto);

    assertThat(priceJSONDto)
        .isNotNull()
        .isEqualTo(expectedPriceJSONDto);
  }
}