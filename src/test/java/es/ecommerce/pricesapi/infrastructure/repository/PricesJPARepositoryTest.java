package es.ecommerce.pricesapi.infrastructure.repository;

import static org.assertj.core.api.Assertions.assertThat;

import es.ecommerce.pricesapi.infrastructure.PostgresContainer;
import es.ecommerce.pricesapi.infrastructure.repository.PricesJPARepository;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@ActiveProfiles("test")
@ContextConfiguration(initializers = {PostgresContainer.Initializer.class})
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class PricesJPARepositoryTest {

  @Autowired
  private PricesJPARepository pricesJPARepository;

  @AfterEach
  void afterEach() {
    pricesJPARepository.deleteAll();
  }

  @Test
  @Sql("/data.sql")
  void should_count_all_the_given_registries() {
    assertThat(pricesJPARepository.count()).isEqualTo(4);
  }

  @Test
  @Sql("/data.sql")
  void should_find_by_brand_and_product_and_application_date() {
    var priceList = pricesJPARepository.findByProductIdAndBrandIdAndApplicationDate(35455L, 1L,
        parseDate("2020-06-14 15:00:00"));
    assertThat(priceList)
        .isNotEmpty()
        .hasSize(2);
  }

  @SneakyThrows
  private Date parseDate(String dateString) {
    DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    return simpleDateFormat.parse(dateString);
  }

}
