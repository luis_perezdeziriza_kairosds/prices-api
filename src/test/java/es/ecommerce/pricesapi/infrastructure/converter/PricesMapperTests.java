package es.ecommerce.pricesapi.infrastructure.converter;

import static org.assertj.core.api.Assertions.assertThat;

import es.ecommerce.pricesapi.domain.model.rest.PriceRequestDto;
import es.ecommerce.pricesapi.domain.model.persistence.PriceTable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;


@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class PricesMapperTests {

  PricesMapper pricesMapper = new PricesMapperImpl();

  @SneakyThrows
  @Test
  void should_convert_entity_to_dto() {
    var entity = PriceTable.builder()
        .brandId(1L)
        .id(1L)
        .currency("EUR")
        .price(BigDecimal.ONE)
        .priority(1L)
        .productId(1L)
        .endDate(getDate("2022-01-02 12:34:55"))
        .startDate(getDate("2022-01-02 12:34:56"))
        .build();

    var priceDto = pricesMapper.fromTableToDto(entity);

    assertThat(priceDto)
        .usingRecursiveComparison()
        .ignoringFields("startDate", "endDate", "currency")
        .isEqualTo(entity);
    assertThat(Date.from(priceDto.getStartDate().toInstant()))
        .isEqualTo(entity.getStartDate().toInstant());
    assertThat(Date.from(priceDto.getEndDate().toInstant()))
        .isEqualTo(entity.getEndDate().toInstant());
  }

  @Test
  void should_convert_input_to_dto() throws ParseException {

    final String dateString = "2022-01-01 12:34:56";

    var priceDto = pricesMapper.fromInputDataToRequestDto(getDate(dateString), 1L, 1L);
    var expectedPriceRequestDto = PriceRequestDto.builder()
        .brandId(1L)
        .productId(1L)
        .applicationDate(getDate(dateString))
        .build();
    assertThat(priceDto).usingRecursiveComparison().isEqualTo(expectedPriceRequestDto);
  }

  public static Date getDate(String date) throws ParseException {
    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    return formatter.parse(date);
  }

}
