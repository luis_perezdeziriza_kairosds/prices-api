package es.ecommerce.pricesapi.infrastructure.controller;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import es.ecommerce.pricesapi.application.usecase.FindPrice;
import es.ecommerce.pricesapi.infrastructure.converter.PricesMapper;
import es.ecommerce.pricesapi.domain.model.rest.PriceJSONDto;
import es.ecommerce.pricesapi.domain.model.rest.PriceRequestDto;
import es.ecommerce.pricesapi.domain.model.error.enums.PricesErrorEnum;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpServerErrorException.InternalServerError;

@WebMvcTest(SwaggerPricesController.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class SwaggerPricesControllerTests {

  public static final String API_PRICES_ENDPOINT = "/prices";
  public static final String DEFAULT_DATE_VALUE = "2022-01-01 12:34:00";

  private static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

  private static final DateFormat DATE_FORMAT =
      new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS);
  private static final DateTimeFormatter DATE_TIME_FORMATTER =
      DateTimeFormatter.ofPattern(YYYY_MM_DD_HH_MM_SS);

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private FindPrice findPrice;

  @MockBean
  private PricesMapper priceMapper;


  @SneakyThrows
  @Test
  void should_return_price_from_service() {

    var defaultStartDateString = "2020-01-01 12:34:56";
    var defaultEndDateString = "2020-01-01 23:45:00";

    var priceDto = PriceJSONDto.builder()
        .brandId(1L)
        .productId(1L)
        .price(BigDecimal.valueOf(12.34))
        .startDate(getZonedDateTime(defaultStartDateString))
        .endDate(getZonedDateTime(defaultEndDateString))
        .build();

    var priceRequestDto = PriceRequestDto.builder()
        .brandId(1L)
        .productId(1L)
        .applicationDate(getDate(defaultStartDateString))
        .build();

    when(findPrice.findPrice(any())).thenReturn(priceDto);
    when(priceMapper.fromInputDataToRequestDto(any(), anyLong(), anyLong())).thenReturn(priceRequestDto);

    mockMvc.perform(
            get(API_PRICES_ENDPOINT)
                .params(buildRequestParams(DEFAULT_DATE_VALUE)))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("2020-01-01 00:00:00")));
  }

  @SneakyThrows
  @Test
  void should_return_missing_parameter_error() {
    var defaultStartDateString = "2020-01-01 12:34:56";

    var priceRequestDto = PriceRequestDto.builder()
        .brandId(1L)
        .productId(1L)
        .applicationDate(getDate(defaultStartDateString))
        .build();

    when(priceMapper.fromInputDataToRequestDto(any(), anyLong(), anyLong())).thenReturn(priceRequestDto);
    mockMvc.perform(MockMvcRequestBuilders
            .get(API_PRICES_ENDPOINT)
            .param("application_date", "2020-01-01 12:34:56")
            .param("brand_id", "1")
            .contentType(MediaType.APPLICATION_JSON))
        .andExpectAll(status().isBadRequest())
        .andExpect(
            jsonPath("$.error", is(PricesErrorEnum.MISSING_REQUEST_PARAMETERS.getCode())))
        .andExpect(
            jsonPath("$.reason", containsString("product_id")));
  }

  @SneakyThrows
  @Test
  void should_return_internal_server_error() {

    when(findPrice.findPrice(any())).thenThrow(InternalServerError.class);

    mockMvc.perform(
            get(API_PRICES_ENDPOINT)
                .params(buildRequestParams(DEFAULT_DATE_VALUE)))
        .andDo(print())
        .andExpect(status().isInternalServerError())
        .andExpect(jsonPath("$.error", is(PricesErrorEnum.INTERNAL_SERVER_ERROR.getCode())));
  }


  @SneakyThrows
  @Test
  void should_return_invalid_parameter_error() {
    mockMvc.perform(MockMvcRequestBuilders
            .get(API_PRICES_ENDPOINT)
            .params(buildRequestParams("applicationDate"))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpectAll(status().isBadRequest())
        .andExpect(
            jsonPath("$.error", is(PricesErrorEnum.INVALID_REQUEST_PARAMETERS.getCode())))
        .andExpect(
            jsonPath("$.reason", containsString("application_date")));
  }

  private MultiValueMap<String, String> buildRequestParams(String applicationDate) {
    MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
    map.add("application_date", applicationDate);
    map.add("product_id", "35455");
    map.add("brand_id", "1");
    return map;
  }

  public static ZonedDateTime getZonedDateTime(String dateString) {
    var localDate = LocalDate.parse(dateString, DATE_TIME_FORMATTER);
    return localDate.atStartOfDay(ZoneId.systemDefault());
  }

  @SneakyThrows
  public static java.util.Date getDate(String date) {
    return DATE_FORMAT.parse(date);
  }
}
