INSERT INTO PRICES(ID, PRODUCT_ID, PRIORITY, START_DATE, END_DATE, PRICE, CURR, BRAND_ID) VALUES(1, 35455, 0, '2020-06-14 0:00:00', '2020-12-31 23:59:59', 35.50, 'EUR', 1);
INSERT INTO PRICES(ID, PRODUCT_ID, PRIORITY, START_DATE, END_DATE, PRICE, CURR, BRAND_ID) VALUES(2, 35455, 1, '2020-06-14 15:00:00', '2020-06-14 18:30:00', 25.45, 'EUR', 1);
INSERT INTO PRICES(ID, PRODUCT_ID, PRIORITY, START_DATE, END_DATE, PRICE, CURR, BRAND_ID) VALUES(3, 35455, 1, '2020-06-15 0:00:00', '2020-06-15 11:00:00', 30.50, 'EUR', 1);
INSERT INTO PRICES(ID, PRODUCT_ID, PRIORITY, START_DATE, END_DATE, PRICE, CURR, BRAND_ID) VALUES(4, 35455, 1, '2020-06-15 16:00:00', '2020-12-31 23:59:59', 38.95, 'EUR', 1);
