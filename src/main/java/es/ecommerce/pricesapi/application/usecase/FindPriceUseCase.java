package es.ecommerce.pricesapi.application.usecase;

import es.ecommerce.pricesapi.infrastructure.converter.PricesMapper;
import es.ecommerce.pricesapi.domain.model.rest.PriceRequestDto;
import es.ecommerce.pricesapi.domain.model.rest.PriceJSONDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@AllArgsConstructor
@Component
public class FindPriceUseCase implements FindPrice {

  private final PricesMapper priceMapper;

  private final es.ecommerce.pricesapi.domain.service.PricesService pricesService;

  @Override
  public PriceJSONDto findPrice(PriceRequestDto priceRequestDto) {
    log.debug("Request DTO: {}", priceRequestDto);

    var pricesServiceDto = priceMapper.fromRequestToService(priceRequestDto);
    log.debug("Service DTO: {}", pricesServiceDto);

    var priceDto = pricesService.findOne(pricesServiceDto);
    log.debug("Price DTO: {}", priceDto);

    var priceJSONDto = priceMapper.fromDtoToJSON(priceDto);
    log.debug("Price JSON format DTO: {}", priceJSONDto);

    return priceJSONDto;
  }
}
