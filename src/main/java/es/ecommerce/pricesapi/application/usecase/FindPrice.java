package es.ecommerce.pricesapi.application.usecase;

import es.ecommerce.pricesapi.domain.model.rest.PriceRequestDto;
import es.ecommerce.pricesapi.domain.model.rest.PriceJSONDto;

public interface FindPrice {

  /**
   * Find the expected price based on the request given
   *
   * @param priceRequestDto given request DTO
   * @return expected price (JSON format)
   */
  PriceJSONDto findPrice(PriceRequestDto priceRequestDto);
}
