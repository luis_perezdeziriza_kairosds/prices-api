package es.ecommerce.pricesapi.infrastructure.controller;

import es.ecommerce.pricesapi.domain.model.rest.PriceJSONDto;
import es.ecommerce.pricesapi.domain.model.error.PricesError;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.util.Date;
import javax.validation.Valid;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/prices")
public interface SwaggerPricesController {

  /**
   * GET method that returns a search-based price
   *
   * @param applicationDate date when the rate is applied
   * @param productId       product ID when the rate is applied
   * @param brandId         brand ID when the rate is applied
   * @return price rate
   */
  @Operation(operationId = "getProperPricing", summary = "Get proper pricing")
  @ApiResponse(responseCode = "200", description = "OK",
      content = {@Content(schema = @Schema(implementation = PriceJSONDto.class))})
  @ApiResponse(responseCode = "400", description = "Bad Request",
      content = {@Content(schema = @Schema(implementation = PricesError.class))})
  @ApiResponse(responseCode = "500", description = "Internal server error",
      content = {@Content(schema = @Schema(implementation = PricesError.class))})
  @GetMapping
  ResponseEntity<PriceJSONDto> getProperPricing(
      @Valid @RequestParam(name = "application_date")
      @Parameter(schema = @Schema(pattern = "yyyy-MM-dd HH:mm:ss"))
      @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") final Date applicationDate,
      @Valid @RequestParam(name = "product_id") final Long productId,
      @Valid @RequestParam(name = "brand_id") final Long brandId);
}
