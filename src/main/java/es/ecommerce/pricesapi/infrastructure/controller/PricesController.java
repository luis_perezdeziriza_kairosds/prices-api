package es.ecommerce.pricesapi.infrastructure.controller;

import es.ecommerce.pricesapi.application.usecase.FindPrice;
import es.ecommerce.pricesapi.infrastructure.converter.PricesMapper;
import es.ecommerce.pricesapi.domain.model.rest.PriceJSONDto;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@AllArgsConstructor
@RestController
public class PricesController implements SwaggerPricesController {

  private final PricesMapper priceMapper;

  private final FindPrice findPrice;

  @Override
  public ResponseEntity<PriceJSONDto> getProperPricing(Date applicationDate, Long productId,
      Long brandId) {

    log.debug("Input data: {} - {} - {}", applicationDate, productId, brandId);
    var priceRequestDto = priceMapper.fromInputDataToRequestDto(applicationDate, productId,
        brandId);
    log.debug("Request DTO: {}", priceRequestDto);

    var priceDto = findPrice.findPrice(priceRequestDto);
    log.debug("Found price DTO: {}", priceDto);

    return new ResponseEntity<>(priceDto, HttpStatus.OK);
  }
}
