package es.ecommerce.pricesapi.infrastructure.repository;

import es.ecommerce.pricesapi.domain.model.persistence.PriceTable;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PricesJPARepository extends JpaRepository<PriceTable, Long> {

  /**
   * Finds prices based on parameters.
   *
   * @param productId       product ID on which the rate is applied
   * @param brandId         brand ID where the rate is applied
   * @param applicationDate rate application date
   * @return price rate
   */
  @Query(value = "from PriceTable t "
      + "where :applicationDate BETWEEN startDate "
      + "AND endDate AND productId = :productId "
      + "AND brandId = :brandId")
  List<PriceTable> findByProductIdAndBrandIdAndApplicationDate(Long productId, Long brandId,
      Date applicationDate);

}
