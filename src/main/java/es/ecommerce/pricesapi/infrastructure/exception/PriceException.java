package es.ecommerce.pricesapi.infrastructure.exception;

public class PriceException extends RuntimeException {

  PriceException(final String message) {
    super(message);
  }

}
