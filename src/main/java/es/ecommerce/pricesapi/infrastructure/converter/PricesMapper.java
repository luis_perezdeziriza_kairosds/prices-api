package es.ecommerce.pricesapi.infrastructure.converter;

import es.ecommerce.pricesapi.domain.model.dto.PriceDto;
import es.ecommerce.pricesapi.domain.model.rest.PriceRequestDto;
import es.ecommerce.pricesapi.domain.model.dto.PriceServiceDto;
import es.ecommerce.pricesapi.domain.model.rest.PriceJSONDto;
import es.ecommerce.pricesapi.domain.model.persistence.PriceTable;
import java.util.Date;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PricesMapper {

  /**
   * Conversion from table to DTO.
   *
   * @param priceTable price entity
   * @return price DTO
   */
  PriceDto fromTableToDto(PriceTable priceTable);

  /**
   * Conversion from input data to request
   *
   * @param applicationDate date where the rate is applied
   * @param productId       product ID
   * @param brandId         brand ID
   * @return price request data DTO
   */
  PriceRequestDto fromInputDataToRequestDto(Date applicationDate, Long productId, Long brandId);

  /**
   * Conversion from request data to service
   *
   * @param priceRequestDto price request DTO
   * @return price service DTO
   */
  PriceServiceDto fromRequestToService(PriceRequestDto priceRequestDto);


  /**
   * Conversion from given price DTO to JSON Format output DTO
   *
   * @param priceDto price DTO
   * @return output DTO (JSON Format)
   */
  PriceJSONDto fromDtoToJSON(PriceDto priceDto);
}
