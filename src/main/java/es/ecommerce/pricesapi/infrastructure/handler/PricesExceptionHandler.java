package es.ecommerce.pricesapi.infrastructure.handler;

import es.ecommerce.pricesapi.domain.model.error.PricesError;
import es.ecommerce.pricesapi.domain.model.error.enums.PricesErrorEnum;
import java.util.Objects;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpServerErrorException.InternalServerError;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class PricesExceptionHandler extends ResponseEntityExceptionHandler {

  /**
   * Response for the raised exception
   *
   * @param ex internal server error exception
   * @return response with error encapsulated
   */
  @ExceptionHandler(InternalServerError.class)
  public ResponseEntity<PricesError> handleInternalErrorException(
      InternalServerError ex) {

    var pricesError = PricesError.builder()
        .error(PricesErrorEnum.INTERNAL_SERVER_ERROR.getCode())
        .message(PricesErrorEnum.INTERNAL_SERVER_ERROR.getError())
        .status(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()))
        .reason(ex.getMessage())
        .build();
    return new ResponseEntity<>(pricesError, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * Response for the raised exception
   *
   * @param ex missing parameter exception
   * @return response with error encapsulated
   */
  @Override
  protected ResponseEntity<Object> handleMissingServletRequestParameter(
      MissingServletRequestParameterException ex,
      @NonNull HttpHeaders headers, @NonNull HttpStatus status, @NonNull WebRequest request) {

    var pricesError = PricesError.builder()
        .error(PricesErrorEnum.MISSING_REQUEST_PARAMETERS.getCode())
        .message(PricesErrorEnum.MISSING_REQUEST_PARAMETERS.getError())
        .status(PricesErrorEnum.INVALID_REQUEST_PARAMETERS.getStatus().name())
        .reason(ex.getMessage())
        .build();

    return new ResponseEntity<>(pricesError, HttpStatus.BAD_REQUEST);
  }

  /**
   * Response for the raised exception
   *
   * @param ex argument type mismatch exception
   * @return response with error encapsulated
   */
  @ExceptionHandler({MethodArgumentTypeMismatchException.class})
  public ResponseEntity<Object> handleTypeMismatch(
      MethodArgumentTypeMismatchException ex) {
    var errorString = ex.getName() + " should be " +
        Objects.requireNonNull(ex.getRequiredType()).getName();

    var pricesError = PricesError.builder()
        .error(PricesErrorEnum.INVALID_REQUEST_PARAMETERS.getCode())
        .message(PricesErrorEnum.INVALID_REQUEST_PARAMETERS.getError())
        .status(PricesErrorEnum.INVALID_REQUEST_PARAMETERS.getStatus().name())
        .reason(errorString)
        .build();

    return new ResponseEntity<>(pricesError, HttpStatus.BAD_REQUEST);
  }

}