package es.ecommerce.pricesapi.domain.service;

import es.ecommerce.pricesapi.domain.model.dto.PriceDto;
import es.ecommerce.pricesapi.domain.model.dto.PriceServiceDto;

public interface PricesService {

  /**
   * Finds the price rate given the input parameters
   *
   * @param priceServiceDto input parameters
   * @return price rate DTO
   */
  PriceDto findOne(PriceServiceDto priceServiceDto);

}