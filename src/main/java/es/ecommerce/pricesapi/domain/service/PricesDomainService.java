package es.ecommerce.pricesapi.domain.service;

import es.ecommerce.pricesapi.domain.repository.PricesRepository;
import es.ecommerce.pricesapi.infrastructure.converter.PricesMapper;
import es.ecommerce.pricesapi.domain.model.dto.PriceDto;
import es.ecommerce.pricesapi.domain.model.dto.PriceServiceDto;
import es.ecommerce.pricesapi.domain.model.persistence.PriceTable;
import java.util.Comparator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@AllArgsConstructor
@Service
public class PricesDomainService implements PricesService {

  private final PricesMapper pricesMapper;

  private final PricesRepository pricesRepository;

  @Override
  public PriceDto findOne(PriceServiceDto priceServiceDto) {

    var priceEntity =
        pricesRepository.findByProductIdAndBrandIdAndApplicationDate(
                priceServiceDto.getProductId(), priceServiceDto.getBrandId(),
                priceServiceDto.getApplicationDate())
            .stream()
            .max(Comparator.comparing(
                PriceTable::getPriority))
            .stream()
            .findFirst()
            .orElse(null);
    log.debug("Price Entity: {}", priceEntity);

    var priceDto = pricesMapper.fromTableToDto(priceEntity);
    log.debug("Price DTO: {}", priceDto);
    return priceDto;
  }
}
