package es.ecommerce.pricesapi.domain.model.dto;

import java.util.Date;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class PriceServiceDto {

  private Long productId;
  private Long brandId;
  private Date applicationDate;
}
