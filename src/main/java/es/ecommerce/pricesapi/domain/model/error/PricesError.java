package es.ecommerce.pricesapi.domain.model.error;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@AllArgsConstructor
@Builder
@Schema(
    name = "prices_error",
    description = "Prices api error object",
    example = "{\"error\":\"error-0002\",\"status\":\"400\",\"message\":\"BAD_REQUEST\",\"reason\":\"Required request parameter 'application_date' for method parameter type Date is not present\"}"
)
public class PricesError {

  @JsonProperty("error")
  @Schema(description = "Error code", example = "error-0002")
  private String error;

  @JsonProperty("message")
  @Schema(description = "Error message", example = "BAD_REQUEST")
  private String message;

  @JsonProperty("status")
  @Schema(description = "Error status", example = "400")
  private String status;

  @JsonProperty("reason")
  @Schema(description = "Reason for the error", example = "Required request parameter 'brand_id' for method parameter type Long is not present")
  private String reason;

}
