package es.ecommerce.pricesapi.domain.model.rest;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@Schema(name = "price", description = "Price object", example = "{\"product_id\":35455,\"brand_id\":1,\"start_date\":\"2020-06-15 16:00:00\",\"end_date\":\"2020-12-31 23:59:59\",\"price\":38.95}")
public class PriceJSONDto implements Serializable {

  @JsonProperty("brand_id")
  @Schema(description = "Brand identifier", example = "1")
  private Long brandId;

  @JsonProperty("start_date")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @Schema(description = "Start date of application of the rate", example = "2020-06-14 15:00:00", pattern = "yyyy-MM-dd HH:mm:ss")
  private ZonedDateTime startDate;

  @JsonProperty("end_date")
  @Schema(description = "End date of application of the rate", example = "2020-06-14 15:00:00", pattern = "yyyy-MM-dd HH:mm:ss")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  private ZonedDateTime endDate;


  @JsonProperty("product_id")
  @Schema(description = "Product identifier", example = "35455")
  private Long productId;


  @JsonProperty("price")
  @Schema(description = "Price", example = "35.52")
  private BigDecimal price;

}
