package es.ecommerce.pricesapi.domain.model.error.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
public enum PricesErrorEnum {
  MISSING_REQUEST_PARAMETERS("E-400", "Bad Request", HttpStatus.BAD_REQUEST),
  INVALID_REQUEST_PARAMETERS("E-400", "Bad Request", HttpStatus.BAD_REQUEST),
  INTERNAL_SERVER_ERROR("E-500", "Internal Server Error", HttpStatus.INTERNAL_SERVER_ERROR);

  @Getter
  private final String code;
  @Getter
  private final String error;
  @Getter
  private final HttpStatus status;

}
