package es.ecommerce.pricesapi.domain.model.rest;

import java.util.Date;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class PriceRequestDto {

  private Long productId;
  private Long brandId;
  private Date applicationDate;

}
