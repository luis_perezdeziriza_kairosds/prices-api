package es.ecommerce.pricesapi.domain.model.dto;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class PriceDto {

  private Long brandId;
  private ZonedDateTime startDate;
  private ZonedDateTime endDate;
  private Long productId;
  private BigDecimal price;

}
