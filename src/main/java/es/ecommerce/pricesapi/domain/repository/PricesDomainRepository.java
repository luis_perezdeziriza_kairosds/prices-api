package es.ecommerce.pricesapi.domain.repository;

import es.ecommerce.pricesapi.domain.model.persistence.PriceTable;
import es.ecommerce.pricesapi.infrastructure.repository.PricesJPARepository;
import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

@Slf4j
@AllArgsConstructor
@Repository
public class PricesDomainRepository implements PricesRepository {

  private final PricesJPARepository pricesJPARepository;

  @Override
  public List<PriceTable> findByProductIdAndBrandIdAndApplicationDate(Long productId, Long brandId,
      Date applicationDate) {
    return pricesJPARepository
        .findByProductIdAndBrandIdAndApplicationDate(productId, brandId, applicationDate);
  }
}
