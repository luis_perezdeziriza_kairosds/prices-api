package es.ecommerce.pricesapi.domain.repository;

import es.ecommerce.pricesapi.domain.model.persistence.PriceTable;
import java.util.Date;
import java.util.List;

public interface PricesRepository {

  List<PriceTable> findByProductIdAndBrandIdAndApplicationDate(
      Long productId, Long brandId, Date applicationDate);

}
