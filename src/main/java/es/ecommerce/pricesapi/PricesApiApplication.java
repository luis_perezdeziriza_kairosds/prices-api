package es.ecommerce.pricesapi;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@OpenAPIDefinition(
    info = @Info(
        title = "prices_v1",
        description = "Querying price(s) API",
        version = "1.0.0")
)
@SpringBootApplication
public class PricesApiApplication {

  public static void main(String[] args) {
    SpringApplication.run(PricesApiApplication.class, args);
  }

}
