# Prices REST API

## Get Prices by brand, product and application date

## Requirements

For building and running the application you need, at least:

- [JDK 1.11](https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html)
- [Maven 3](https://maven.apache.org)
- [Lombok](https://projectlombok.org/)

It is recommended to install [SDKMan!](https://sdkman.io/) to properly deal with those different
versions.

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute
the `main` method in the `es.ecommerce.pricesapi.PricesApiApplication` class from your IDE.

Alternatively you can use
the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html)
like so:

```shell
mvn spring-boot:run
```

## API Reference

#### Get all prices for a given date

```http
  GET /prices
```

| Parameter          | Type     | Description                                                                         |
|:-------------------|:---------|:------------------------------------------------------------------------------------|
| `application_date` | `string` | **Required**.<br/>Price Application Date and Time <br/>(yyyy-MM-dd HH:mm:ss format) |
| `product_id`       | `string` | **Required**. Product ID                                                            |
| `brand_id`         | `string` | **Required**. Brand ID                                                              |

## Usage/Examples

```sh
curl -i -H 'Accept: application/json' http://localhost:8080/prices?application_date=2020-06-14%2015%3A01%3A00&product_id=35455&brand_id=1
```

### Example Response

```json
{"brand_id":1,"start_date":"2020-06-14 00:00:00","end_date":"2020-12-31 23:59:59","product_id":35455,"price":35.50}
```

## Running the tests

```sh
mvn clean verify
```


## Feedback

If you have any feedback, please [reach me](mailto:luis.perezdeziriza@kairosds.com?subject=[Gitlab]%20Prices%20API).

